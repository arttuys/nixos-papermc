{ config, lib, pkgs, ... }:

with lib;

let
    # This is quite a bit simplified version from the module in "https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/services/games/minecraft-server.nix"
    # Declarative support has essentially been omitted, considering Paper adds a significant amount of configuration and it is not currently
    # in personal interest to implement full declarative support for it. Perhaps later?

    # What we refer to as our configuration?    
    cfg = config.services.papermc-server;

    # EULA autofill
    eulaFile = builtins.toFile "eula.txt" ''
    # eula.txt confirmed via NixOS configuration
    eula=true
    '';
in
{
    options = {
        services.papermc-server = {
            enable = mkOption {
                type = types.bool;
                default = false;
                description = ''
                Whether to enable Paper MC server
                '';
            };

             eula = mkOption {
                type = types.bool;
                default = false;
                description = ''
                Whether you agree to
                <link xlink:href="https://account.mojang.com/documents/minecraft_eula">
                Mojangs EULA</link>. This option must be set to
                <literal>true</literal> to run PaperMC.
                '';
            };

            dataDir = mkOption {
                type = types.path;
                default = "/var/lib/papermc";
                description = ''
                Directory to store Paper server files and world state in.
                '';
            };

            openFirewall = mkOption {
                type = types.bool;
                default = false;
                description = ''
                Whether to open port TCP/25565 in the firewall for PaperMC. If your server configuration diverges
                from this, you need to use an alternative method.
                '';
            };

            package = mkOption {
                type = types.package;
                default = pkgs.papermc-server;
                defaultText = "pkgs.papermc-server";
                description = "PaperMC package to run. By default, this is <literal>pkgs.papermc-server</literal>";
            };

            jvmOpts = mkOption {
                type = types.separatedString " ";
                default = "-Xmx2048M -Xms2048M";
                description = "JVM options for the server.";
            };
        };
    };

    # If we enabled our server
    config = mkIf cfg.enable {
        # Create an user for us
        users.users.papermc = {
            description     = "PaperMC server service user";
            home            = cfg.dataDir;
            createHome      = true;
            # Since this is a custom overlay package, we don't have an allocated UID. However, Minecraft does so we (re)use that
            uid             = config.ids.uids.minecraft;
        };

        systemd.services.papermc-server = {
            description   = "PaperMC Server Service";
            wantedBy      = [ "multi-user.target" ];
            after         = [ "network.target" ];

            serviceConfig = {
                ExecStart = "${cfg.package}/bin/paper-server ${cfg.jvmOpts}";
                Restart = "always";
                User = "papermc";
                WorkingDirectory = cfg.dataDir;
            };

            preStart = ''
                ln -sf ${eulaFile} eula.txt
            '';
        };

        # If requested, open the firewall for this
        networking.firewall = mkIf cfg.openFirewall {
            allowedTCPPorts = [ 25565 ];
        };

        # Ensure that acceptance to EULA is explicitly declared
        assertions = [
            { assertion = cfg.eula;
                message = "You must agree to Mojang's EULA to run papermc-server."
                + " Read https://account.mojang.com/documents/minecraft_eula and"
                + " set `services.papermc-server.eula` to `true` if you agree.";
            }
        ];
    };
}