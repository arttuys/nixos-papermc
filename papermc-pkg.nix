  
{ stdenv, fetchurl, jre }:
let
    # Use these to specify the applying version for this specific package.
    paper_mc_version = "1.15.2";
    paper_mc_build = "143";

    paper_mc_hash = "0qadwys7ld3qjswvnnp04878zdbyyfqhmccsc9w8xylf8f7bipiy";
in
stdenv.mkDerivation rec {
    pname = "paper-mc";
    version = "${paper_mc_version}-${paper_mc_build}";

    src = fetchurl {
        url = "https://papermc.io/api/v1/paper/${paper_mc_version}/${paper_mc_build}/download";
        sha256 = paper_mc_hash;
        name = "paper-${paper_mc_version}-${paper_mc_build}.jar";
    };

    # This only needs a download. Nothing more.
    preferLocalBuild = true;

    # We only require an install phase
    phases = "installPhase";

    # As mocked from the original MC script; create a storage directory, and a wrapper
    installPhase = ''
        mkdir -p $out/bin $out/lib/paper
        cp -v $src $out/lib/paper/server.jar

        cat > $out/bin/paper-server << EOF
        #!/bin/sh
        exec ${jre}/bin/java \$@ -jar $out/lib/paper/server.jar nogui
        EOF

        chmod +x $out/bin/paper-server
    '';
}