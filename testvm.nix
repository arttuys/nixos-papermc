{ pkgs, lib, config, ... }:
{
  imports = [
     ./papermc-module.nix
  ];

  nixpkgs.overlays = [
      (import ./papermc-overlay.nix)
  ];

  users.users.root.initialPassword = "root";
  systemd.services."serial-getty@ttyS0".enable = true;

  services.papermc-server.enable = true;
  services.papermc-server.eula = true;
  services.papermc-server.jvmOpts = "-Xmx512M -Xms512M";

  networking.firewall.enable = false;
}